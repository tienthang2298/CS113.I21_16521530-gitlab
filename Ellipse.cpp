#include "Ellipse.h"

void Draw4Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
	SDL_RenderDrawPoint(ren, xc+x, yc+y);
    SDL_RenderDrawPoint(ren, xc-x, yc+y);
    SDL_RenderDrawPoint(ren, xc-x, yc-y);
    SDL_RenderDrawPoint(ren, xc+x, yc-y);
}

void BresenhamDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{
	int a2 = a*a, b2 = b*b;
	int p = -2*a2*b + a2 + 2*b2;
	int x = 0, y = b;
    // Area 1
	while (x*x*(a2 + b2) <= a2*a2) {
        Draw4Points(xc, yc, x, y, ren);
        if (p <= 0) p += 4*b2*x + 6*b2;
        else {
            p += 4*b2*x - 4*a2*y;
            y--;
        }
        x++;		
    }
    // Area 2
    p = -2*a*b2 + b2 + 2*a2;
    x = a, y = 0;
    while (x*x*(a2 + b2) >= a2*a2) {
    	Draw4Points(xc, yc, x, y, ren);
    	if (p <= 0) p += 4*a2*y + 6*a2;
    	else {
    		p += 4*a2*y - 4*b2*x;
    		x--;
    	}
    	y++;
    }
}

void MidPointDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{
    // Area 1
    int a2 = a*a, b2 = b*b;
    int p = b2 - a2*b + a2/4;
    int x = 0, y = b;
    while (x*x*(a2 + b2) <= a2*a2) {
        Draw4Points(xc, yc, x, y, ren);
        if (p <= 0) p += 2*b2*x + 3*b2;
        else {
            p += 2*b2*x - 2*a2*y + 3*b2 + 2*a2;
            y--;
        }
        x++;
    }
    // Area 2
    p = a2 - a*b2 + b2/4;
    x = a, y = 0;
    while (x*x*(a2 + b2) >= a2*a2) {
        Draw4Points(xc, yc, x, y, ren);
        if (p <= 0) p += 2*a2*y + 3*a2;
        else {
            p += 2*a2*y - 2*b2*x + 3*a2 + 2*b2;
            x--;
        }
        y++;
    }

}