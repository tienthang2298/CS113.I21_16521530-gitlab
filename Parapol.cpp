#include "Parapol.h"
#include "math.h"

void Draw2Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
    //draw 2 points
    SDL_RenderDrawPoint(ren, xc - x, yc - y);
    SDL_RenderDrawPoint(ren, xc + x, yc - y);
}

void Draw2PointsForParapolNegative(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
    SDL_RenderDrawPoint(ren, xc - x, yc + y);
    SDL_RenderDrawPoint(ren, xc + x, yc + y);
}


void BresenhamDrawParapolPositive(int xc, int yc, int A, SDL_Renderer *ren)
{
	int x1 = 0;
    int y1 = 0;
    int p1 = 1 - A;
    while (x1 < A)
    {
        Draw2PointsForParapolNegative(xc,yc,x1,y1,ren);
        if (p1 <= 0) p1 = p1 + 2*x1 + 3;
        else
        {
            y1++;
            p1 = p1 + 2*x1 + 3 - 2*A;
        }
        x1++;
    }

    int x2 = A;
    int y2 = A/2;

    int p2 = 2*A - 1;

    while(y2 < 4000)
    {
        Draw2PointsForParapolNegative(xc,yc,x2,y2,ren);
        if (p2 <= 0) p2 = p2 + 4*A;
        else
        {            
            p2 = p2 + 4*A - 4*x2 - 4;
            x2++;
        }
        y2 ++;
    }
}

void BresenhamDrawParapolNegative(int xc, int yc, int A, SDL_Renderer *ren)
{
	int x1 = 0;
    int y1 = 0;
    int p1 = 1 - A;
    while (x1 < A)
    {
        Draw2Points(xc,yc,x1,y1,ren);
        if (p1 <= 0) p1 = p1 + 2*x1 + 3;
        else
        {
            y1++;
            p1 = p1 + 2*x1 + 3 - 2*A;
        }
        x1++;
    }

    int x2 = A;
    int y2 = A/2;

    int p2 = 2*A - 1;

    while(y2 < 4000)
    {
        Draw2Points(xc,yc,x2,y2,ren);
        if (p2 <= 0) p2 = p2 + 4*A;
        else
        {             	      
            p2 = p2 + 4*A - 4*x2 - 4;
            x2++;
        }
        y2 ++;
    }
}