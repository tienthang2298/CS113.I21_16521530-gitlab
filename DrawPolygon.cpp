#include "DrawPolygon.h"
#include <iostream>
using namespace std;

void DrawEquilateralTriangle(int xc, int yc, int R, SDL_Renderer *ren)
{
	float anpha = M_PI/2;
	int x = xc + R*cos(anpha), y = yc - R*sin(anpha);
	anpha += 2*M_PI/3;
	for (int i = 0; i < 3; i++) {
		int X = xc + int((float)R * cos(anpha) + 0.5),
			Y = yc - int((float)R * sin(anpha) + 0.5);
		Bresenham_Line(x, y, X, Y, ren);			
		x = X; y = Y;
		anpha += 2*M_PI/3;
	}
}

void DrawSquare(int xc, int yc, int R, SDL_Renderer *ren)
{
	float anpha = M_PI/4;
	int x = xc + R*cos(anpha), y = yc - R*sin(anpha);
	anpha += M_PI/2;
	for (int i = 0; i < 5; i++) {
		int X = xc + int((float)R * cos(anpha) + 0.5),
			Y = yc - int((float)R * sin(anpha) + 0.5);
		Bresenham_Line(x, y, X, Y, ren);		
		x = X; y = Y;
		anpha += M_PI/2;
	}
}
void DrawPentagon(int xc, int yc, int R, SDL_Renderer *ren)
{
	float anpha = M_PI/2;
	int x = xc + R*cos(anpha), y = yc - R*sin(anpha);
	anpha += 2*M_PI/5;
	for (int i = 0; i < 6; i++) {
		int X = xc + int((float)R * cos(anpha) + 0.5),
			Y = yc - int((float)R * sin(anpha) + 0.5);
		Bresenham_Line(x, y, X, Y, ren);		
		x = X; y = Y;
		anpha += 2*M_PI/5;
	}
}
void DrawHexagon(int xc, int yc, int R, SDL_Renderer *ren)
{
	float anpha = 0;
	int x = xc + R*cos(anpha), y = yc - R*sin(anpha);
	anpha += M_PI/3;
	for (int i = 0; i < 7; i++) {
		int X = xc + int((float)R * cos(anpha) + 0.5),
			Y = yc - int((float)R * sin(anpha) + 0.5);
		Bresenham_Line(x, y, X, Y, ren);
		x = X; y = Y;
		anpha += M_PI/3;
	}
}

void DrawStar(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[5], y[5], xp[5], yp[5];
	float anpha = M_PI/2, r = sin(M_PI/10)*R/sin(7*M_PI/10);
	for (int i = 0; i < 5; i++) {
		x[i] = xc + int(R*cos(anpha) + 0.5);
		y[i] = yc - int(R*sin(anpha) + 0.5);

		xp[i] = xc + int(r*cos(anpha + M_PI/5) + 0.5);
		yp[i] = yc - int(r*sin(anpha + M_PI/5) + 0.5);

		anpha += 2*M_PI/5;
	}
	for (int i = 0; i < 5; i++) {
		Bresenham_Line(x[i], y[i], xp[i], yp[i], ren);
		Bresenham_Line(x[(i + 1)%5], y[(i + 1)%5], xp[i], yp[i], ren);
	}
	anpha = 3*M_PI/2;
	int xx = xc + r*cos(anpha), yy = yc - r*sin(anpha);
	anpha += 2*M_PI/5;
	for (int i = 0; i < 6; i++) {
		int X = xc + int((float)r * cos(anpha) + 0.5),
			Y = yc - int((float)r * sin(anpha) + 0.5);
		Bresenham_Line(xx, yy, X, Y, ren);		
		xx = X; yy = Y;
		anpha += 2*M_PI/5;
	}
}

void DrawEmptyStar(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[5], y[5], xp[5], yp[5];
	float anpha = M_PI/2, r = sin(M_PI/10)*R/sin(7*M_PI/10);
	for (int i = 0; i < 5; i++) {
		x[i] = xc + int(R*cos(anpha) + 0.5);
		y[i] = yc - int(R*sin(anpha) + 0.5);

		xp[i] = xc + int(r*cos(anpha + M_PI/5) + 0.5);
		yp[i] = yc - int(r*sin(anpha + M_PI/5) + 0.5);

		anpha += 2*M_PI/5;
	}
	for (int i = 0; i < 5; i++) {
		Bresenham_Line(x[i], y[i], xp[i], yp[i], ren);
		Bresenham_Line(x[(i + 1)%5], y[(i + 1)%5], xp[i], yp[i], ren);
	}
}

//Star with eight wings
void DrawStarEight(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[8], y[8], xp[8], yp[8];
	float anpha = 0, r = sin(M_PI/8)*R/sin(3*M_PI/4);
	for (int i = 0; i < 8; i++) {
		x[i] = xc + int(R*cos(anpha) + 0.5);
		y[i] = yc - int(R*sin(anpha) + 0.5);

		xp[i] = xc + int(r*cos(anpha + M_PI/8) + 0.5);
		yp[i] = yc - int(r*sin(anpha + M_PI/8) + 0.5);

		anpha += M_PI/4;
	}
	for (int i = 0; i < 8; i++) {
		Bresenham_Line(x[i], y[i], xp[i], yp[i], ren);
		Bresenham_Line(x[(i + 1)%8], y[(i + 1)%8], xp[i], yp[i], ren);
	}
}

//For drawing one star of convergent star
void DrawStarAngle(int xc, int yc, int R, float startAngle, SDL_Renderer *ren)
{
	int x[5], y[5], xp[5], yp[5];
	float anpha = startAngle, r = sin(M_PI/10)*R/sin(7*M_PI/10);
	for (int i = 0; i < 5; i++) {
		x[i] = xc + int(R*cos(anpha) + 0.5);
		y[i] = yc - int(R*sin(anpha) + 0.5);

		xp[i] = xc + int(r*cos(anpha + M_PI/5) + 0.5);
		yp[i] = yc - int(r*sin(anpha + M_PI/5) + 0.5);

		anpha += 2*M_PI/5;
	}
	for (int i = 0; i < 5; i++) {
		Bresenham_Line(x[i], y[i], xp[i], yp[i], ren);
		Bresenham_Line(x[(i + 1)%5], y[(i + 1)%5], xp[i], yp[i], ren);
	}
}

void DrawConvergentStar(int xc, int yc, int r, SDL_Renderer *ren)
{
	float anpha = M_PI/2;
	while (r > 1) {		
		DrawStarAngle(xc, yc, r, anpha, ren);
		anpha += M_PI/5;
		r = sin(M_PI/10)*r/sin(7*M_PI/10);
	}
}
