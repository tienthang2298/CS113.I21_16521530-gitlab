#include "Clipping.h"

RECT CreateWindow(int l, int r, int t, int b)
{
    RECT rect;
    rect.Left = l;
    rect.Right = r;
    rect.Top = t;
    rect.Bottom = b;

    return rect;
}

CODE Encode(RECT r, Vector2D P)
{
    CODE c = 0;
    if (P.x < r.Left)
        c = c|LEFT;
    if (P.x > r.Right)
        c = c|RIGHT;
    if (P.y < r.Top)
        c = c|TOP;
    if (P.y > r.Bottom)
        c = c|BOTTOM;
    return c;
}

int CheckCase(int c1, int c2)
{
    if (c1 == 0 && c2 == 0)
        return 1;
    if (c1 != 0 && c2 != 0 && (c1&c2) != 0)
        return 2;
    return 3;
}

int CohenSutherland(RECT r, Vector2D P1, Vector2D P2, Vector2D &Q1, Vector2D &Q2)
{        
    int c1 = Encode(r, P1), c2 = Encode(r, P2);
    int th = CheckCase(c1, c2);
    while (th == 3) {
        ClippingCohenSutherland(r, P1, P2);
        c1 = Encode(r, P1), c2 = Encode(r, P2);
        th = CheckCase(c1, c2);
    }
    if (th == 2) return 0;
    Q1 = P1;
    Q2 = P2;
    return 1;
}

void ClippingCohenSutherland(RECT r, Vector2D &P1, Vector2D &P2)
{
    int pos = Encode(r, P1);
    if (pos == 0) {
        Vector2D q = P1;
        P1 = P2;
        P2 = q;
    }
    int dx = P2.x - P1.x, dy = P2.y - P1.y;
    float m = 0; 
    if (dx != 0) m = (float)dy/dx;
    if (pos & 1) {
        P1.y += int(m*(r.Left - P1.x) + 0.5);
        P1.x = r.Left;
        return;
    }
    if (pos & 2) {
        P1.y += int(m*(r.Right - P1.x) + 0.5); 
        P1.x = r.Right;
        return;
    }
    if (pos & 4) {
        if (dx != 0) P1.x += int((r.Bottom - P1.y)/m + 0.5);
        P1.y = r.Bottom;
        return;
    }
    if (pos & 8) {
        if (dx != 0) P1.x += int((r.Top - P1.y)/m + 0.5);
        P1.y = r.Top;
        return;
    }
    return;
}

int SolveNonLinearEquation(int p, int q, float &t1, float &t2)
{
    if (p == 0)
    {
        if (q < 0)
            return 0;
        return 1;
    }

    if (p > 0)
    {
        float t=(float)q/p;
        if(t2<t)
            return 1;
        if(t<t1)
            return 0;
        t2 = t;
        return 1;
    }

    float t=(float)q/p;
    if(t2<t)
        return 0;
    if(t<t1)
        return 1;
    t1 = t;
    return 1;
}

int LiangBarsky(RECT r, Vector2D P1, Vector2D P2, Vector2D &Q1, Vector2D &Q2)
{
    int dx = P2.x - P1.x, dy = P2.y - P1.y;
    int xmin = r.Left, xmax = r.Right, ymin = r.Top, ymax = r.Bottom;
    int p1 = -dx, p2 = dx, p3 = -dy, p4 = dy;
    int q1 = P1.x - xmin, q2 = xmax - P1.x;
    int q3 = P1.y - ymin, q4 = ymax - P1.y;
    float t1 = 0, t2 = 1;
    int a = SolveNonLinearEquation(p1, q1, t1, t2);
    int b = SolveNonLinearEquation(p2, q2, t1, t2);
    int c = SolveNonLinearEquation(p3, q3, t1, t2);
    int d = SolveNonLinearEquation(p4, q4, t1, t2);    
    if (a * b * c * d == 0) return 0;
    Q1.x = P1.x + int(t1*dx + 0.5);
    Q1.y = P1.y + int(t1*dy + 0.5);
    Q2.x = P1.x + int(t2*dx + 0.5);
    Q2.y = P1.y + int(t2*dy + 0.5);
    return 0;
}
